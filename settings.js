/**
	Startpage Reworked
	==================

	by Barlend,
	
*/



$(document).ready(function() {

/**
	Simple configuration using (mostly) true/false settings. Use "true" to enable
	feature, "false" to disable feature.
*/

	// Open in new window_tab
	newwindow = true;

	// Enable/disable various search engines
	google = true;
	googleimages = false;
	yahoo = false;
	wikipedia = false;
	dictcc = false;
	leo = false;
	flickr = false;
	deviantart = false;

  // Focus on searchbox when opening
    focusSearch = false;

});
